<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>添加/修改 订单-物料-计划</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <#include "${request.contextPath}/common/common.ftl">
    <link href="${request.contextPath}/css/effect.css" rel="stylesheet" type="text/css"/>

    <style>

    </style>

</head>
<body>
<div class="splayui-container">
    <div class="splayui-main">
        <form class="layui-form splayui-form" lay-filter="formTest">
            <div class="layui-row">
                <div class="layui-col-xs6 layui-col-sm6 layui-col-md6">



                    <div class="layui-form-item">
                        <label for="js-line" class="layui-form-label sp-required">工艺线路
                        </label>
                        <div class="layui-input-inline">
                            <select  id="js-line" name="line" lay-filter="line-filter" lay-verify="required" autocomplete="off" class="layui-input" value="${result.line}">
                            </select>
                        </div>
                    </div>



                    <div class="layui-form-item">
                        <label for="js-orderCode" class="layui-form-label sp-required"  >工单编码
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-orderCode" name="orderCode" lay-verify="required"
                                   autocomplete="off" class="layui-input" value="${result.orderCode}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                            <label for="js-bomCode" class="layui-form-label sp-required">BOM编码
                            </label>
                            <div class="layui-input-inline">
                                <select  id="js-bomCode" name="bomCode" lay-filter="bomCode-filter" lay-verify="required" autocomplete="off" class="layui-input" value="${result.bomCode}">
                                </select>
                            </div>

                    </div>

                    <div class="layui-form-item">
                        <label for="js-materialCode" class="layui-form-label sp-required">物料编码
                        </label>
                        <div style="display: flex;   flex-direction: row;">
                            <button type="button" id="js-test-btn" class="layui-btn" style="height:37px "><i
                                        class="layui-icon layui-icon-search "></i>
                            </button>
                            <input id="js-materialCode" name="materialCode"  lay-verify="required"
                                   placeholder="搜索物料" autocomplete="off"
                                   value="${result.materialCode}"
                                   class="layui-input" style="width: 133PX">
                        </div>
                    </div>


            <#--        <div class="layui-form-item">
                        <label for="js-batchNo" class="layui-form-label sp-required">物料批次
                        </label>
                        <div style="display: flex;   flex-direction: row;">
                            <button type="button" id="js-batchNo-btn" class="layui-btn" style="height:37px "><i
                                        class="layui-icon layui-icon-search "></i>
                            </button>
                            <input id="js-batchNo" name="batchNo"
                                   placeholder="搜索物料批次" autocomplete="off"
                                   value="${result.batchNo}"
                                   class="layui-input" style="width: 133PX">
                        </div>
                    </div>

-->

                    <div class="layui-form-item">
                         <label for="js-batchNo" class="layui-form-label sp-required">物料批次
                         </label>
                         <div class="layui-input-inline">
                             <input type="text" id="js-batchNo" name="batchNo"
                                    autocomplete="off" class="layui-input" value="${result.batchNo}">
                         </div>
                     </div>

                    <div class="layui-form-item">
                        <label for="js-amount" class="layui-form-label sp-required">  该批次数量
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-amount" name="amount"
                                   autocomplete="off" class="layui-input" value="${result.amount}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-realAmount" class="layui-form-label ">  该批次实际用量
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-realAmount" name="realAmount"   <#--readonly="false"-->
                                   autocomplete="off" class="layui-input" value="${result.realAmount}">
                        </div>
                    </div>

                </div>

                <div class="layui-form-item layui-hide">
                    <div class="layui-input-block">
                        <input id="js-id" name="id" value="${result.id}"/>
                        <button id="js-submit" class="layui-btn" lay-submit lay-filter="js-submit-filter">确定
                        </button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
<script>

    layui.use(['form', 'util', 'layer', 'step', 'spLayer', 'spTable', 'table'], function () {
        var form = layui.form,
            spLayer = layui.spLayer,
            layer = layui.layer,
            table = layui.table,
            util = layui.util;


        //监听提交
        form.on('submit(js-submit-filter)', function (data) {
            //alert( JSON.stringify(data.field));

            spUtil.submitForm({
                //contentType: 'application/json;charset=UTF-8',  //@RequestBody  要对应起来，contentType默认按 x-from 处理得
                url: "${request.contextPath}/order/orderMaterial/add-or-update",
                data: data.field
            });
            return false;
        });

        function getFlowData() {
            spUtil.ajax({
                url: '${request.contextPath}/basedata/flow/list',
                async: false,
                type: 'GET',
                // 是否显示 loading
                // showLoading: true,
                // 是否序列化参数
                serializable: false,
                // 参数
                data: {},
                success: function (data) {
                    $.each(data.data, function (index, item) {
                        $('#js-line').append(new Option(item.flow, item.flow));
                    });
                    //document.getElementById("js-line")[0].selected=true;
                }
            });

        }


        function getBomData() {
            spUtil.ajax({
                url: '${request.contextPath}/technology/bom/GetBomList',
                async: false,
                type: 'GET',
                // 是否显示 loading
                // showLoading: true,
                // 是否序列化参数
                serializable: false,
                // 参数
                data: {},
                success: function (data) {
                    $.each(data.data, function (index, item) {
                        $('#js-bomCode').append(new Option(item, item));
                    });

                }

            });

        }


        function getOrderData() {
            spUtil.ajax({
                url: '${request.contextPath}/order/getOrderData',
                async: false,
                type: 'GET',
                // 是否显示 loading
                // showLoading: true,
                // 是否序列化参数
                serializable: false,
                // 参数
                data: {},
                success: function (data) {
                    $.each(data.data, function (index, item) {
                        $('#js-orderCode').append(new Option(item, item));
                    });
                }
            });

        }

        getFlowData();
        getBomData();
        //getOrderData();

        //给表单赋值
        form.val("formTest", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值
            "line": "${result.line}",
            "bomCode": "${result.bomCode}"
            //"orderCode": "${result.orderCode}"
        });


        // 物料主数据搜索弹框
        $('#js-test-btn').click(function () {
            searchMaterial();
        });
        window.searchMaterial = function (obj) {
            spLayer.open({
                type: 2,
                area: ['680px', '500px'],
                reload: false,
                content: '${request.contextPath}/admin/common/ui/searchPanelMaterile',
                // 如果是搜索弹窗，需要添加回调函数来获取选中数据
                spCallback: function (result) {
                    if (result.code === 0 && result.data.length > 0) {
                        console.log(result);
                        $('#js-materialCode').val(result.data[0].materiel);
                        //$('#js-materiel-name').val(result.data[0].materielDesc);
                        $('#js-batchNo').val(result.data[0].batchNo);

                    }
                }
            });
        }



/*

        // 物料批次数据搜索弹框
        $('#js-batchNo-btn').click(function () {
            searchBatchNo();
        });
        window.searchBatchNo = function (obj) {
            spLayer.open({
                type: 2,
                area: ['680px', '500px'],
                reload: false,
                content: '${request.contextPath}/admin/common/ui/searchPanelMaterileBatchNo',
                // 如果是搜索弹窗，需要添加回调函数来获取选中数据
                spCallback: function (result) {
                    if (result.code === 0 && result.data.length > 0) {
                        console.log(result);
                        $('#js-batchNo').val(result.data[0].batchNo);

                    }
                }
            });
        }

*/


    });


    window.onload=function(){
        document.getElementById("js-amount").value = document.getElementById("js-amount").value.replace(/,/g,"");
    }


</script>
</body>
</html>

