<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>添加/修改device</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <#include "${request.contextPath}/common/common.ftl">
    <link href="${request.contextPath}/css/effect.css" rel="stylesheet" type="text/css"/>

    <style>

    </style>

</head>
<body>
<div class="splayui-container">
    <div class="splayui-main">
        <form class="layui-form splayui-form" lay-filter="formTest">
            <div class="layui-row">
                <div class="layui-col-xs6 layui-col-sm6 layui-col-md6">

                    <div class="layui-form-item">
                        <label for="js-material" class="layui-form-label sp-required">物料编码
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-material" name="material" lay-verify="required" autocomplete="off"
                                   class="layui-input" value="${result.material}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-batchNo" class="layui-form-label ">批次
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-batchNo" name="batchNo"
                                   autocomplete="off" class="layui-input" value="${result.batchNo}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-amount" class="layui-form-label ">数量
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-amount" name="amount"
                                   autocomplete="off" class="layui-input" value="${result.amount}">
                        </div>
                    </div>


                    <div class="layui-form-item">
                        <label for="js-unit" class="layui-form-label ">物料单位
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-unit" name="unit"
                                   autocomplete="off" class="layui-input" value="${result.unit}">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label for="js-handler" class="layui-form-label ">领料人
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-handler" name="handler"
                                   autocomplete="off" class="layui-input" value="${result.handler}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-houseOut" class="layui-form-label ">出库仓
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-houseOut" name="houseOut"
                                   autocomplete="off" class="layui-input" value="${result.houseOut}">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label for="js-houseIn" class="layui-form-label ">入库仓
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-houseIn" name="houseIn"
                                   autocomplete="off" class="layui-input" value="${result.houseIn}">
                        </div>
                    </div>


                </div>

                <div class="layui-form-item layui-hide">
                    <div class="layui-input-block">
                        <input id="js-id" name="id" value="${result.id}"/>
                        <button id="js-submit" class="layui-btn" lay-submit lay-filter="js-submit-filter">确定
                        </button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['form', 'util'], function () {
        var form = layui.form,
            util = layui.util;

        //监听提交
        form.on('submit(js-submit-filter)', function (data) {
            //alert( JSON.stringify(data.field));

            spUtil.submitForm({
                //contentType: 'application/json;charset=UTF-8',  //@RequestBody  要对应起来，contentType默认按 x-from 处理得
                url: "${request.contextPath}/material/fetch/add-or-update",
                data: data.field
            });
            return false;
        });

    });

    function handleNumber() {
        $("#js-amount").value= $("#js-amount").value.replace(/,/g,"");

    }

    window.onload=function() {
        document.getElementById("js-amount").value = document.getElementById("js-amount").value.replace(/,/g, "");
    }

</script>
</body>
</html>

