<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>添加/修改报工</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <#include "${request.contextPath}/common/common.ftl">
    <link href="${request.contextPath}/css/effect.css" rel="stylesheet" type="text/css"/>
    <style>
        .flowProcss {
            font-size: 1.6rem;
            margin-left: 310PX;
            display: flex;
            justify-content: flex-start;
            flex-direction: row;
        }

    </style>

</head>
<body>
<div class="splayui-container">
    <div class="splayui-main">
        <form class="layui-form splayui-form" lay-filter="formTest">
            <div class="layui-row">

                <#---------左边------>
                <div class="layui-col-xs6 layui-col-sm6 layui-col-md6">


                    <div class="layui-form-item">
                        <label for="js-reportCode" class="layui-form-label ">报工单编码
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-reportCode" name="reportCode" lay-verify="" autocomplete="off"
                               class="layui-input" value="${result.reportCode}"     autofocus="autofocus" >

                        </div>
                    </div>

                   <div class="layui-form-item">
                        <label for="js-orderCode" class="layui-form-label ">订单编码
                        </label>
                        <div class="layui-input-inline">
                            <select id="js-orderCode" name="orderCode" lay-filter="orderCode-filter">
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-planCode" class="layui-form-label ">计划编码
                        </label>
                        <div class="layui-input-inline">
                            <select id="js-planCode" name="planCode" lay-filter="planCode-filter">
                            </select>
                        </div>
                    </div>



                    <div class="layui-form-item">
                        <label for="js-useTime" class="layui-form-label ">报工时长（分钟）
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-useTime" name="useTime" autocomplete="off"
                                    <#--readonly="false"-->  class="layui-input" value="${result.useTime}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-makedQty" class="layui-form-label ">报工数量
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-makedQty" name="makedQty"
                                    <#--readonly="false"-->   autocomplete="off" class="layui-input" value="${result.makedQty}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-badQty" class="layui-form-label ">次品数量
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-badQty" name="badQty" lay-verify=""
                                   autocomplete="off"  <#--readonly="true"-->
                                   class="layui-input" value="${result.badQty}">
                        </div>
                    </div>

                </div>

                <div class="layui-form-item layui-hide">
                    <div class="layui-input-block">
                        <input id="js-id" name="id" value="${result.id}"/>
                        <button id="js-submit" class="layui-btn" lay-submit lay-filter="js-submit-filter">确定
                        </button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
<script>
    layui.use(['form', 'util'], function () {
        var form = layui.form,
            util = layui.util;

        //添加下拉框
        geOrderData();
        getPlanData();

        $('#js-reportCode').focus();

        //给表单赋值
        form.val("formTest", {

            "orderCode": "${result.orderCode}",
            "planCode": "${result.planCode}"

        });

        //监听提交
        form.on('submit(js-submit-filter)', function (data) {
            //alert( JSON.stringify(data.field));

            spUtil.submitForm({
                url: "${request.contextPath}/workReport/add-or-update",
                data: data.field
            });
            return false;
        });

    });


    function geOrderData() {
        spUtil.ajax({
            url: '${request.contextPath}/order/list',
            async: false,
            type: 'GET',
            // 是否显示 loading
            showLoading: true,
            // 是否序列化参数
            serializable: false,
            // 参数
            data: {},
            success: function (data) {
                typeRows = data.data;

                $.each(typeRows, function (index, item) {
                    $('#js-orderCode').append(new Option(item.orderCode, item.orderCode));
                });

            }
        });

    }

    function getPlanData() {
        spUtil.ajax({
            url: '${request.contextPath}/daily/plan/list',
            async: false,
            type: 'GET',
            // 是否显示 loading
            showLoading: true,
            // 是否序列化参数
            serializable: false,
            // 参数
            data: {},
            success: function (data) {
                $.each(data.data, function (index, item) {

                    $('#js-planCode').append(new Option(item.planCode, item.planCode));
                });

            }
        });


    }

    window.onload=function(){
        document.getElementById("js-makedQty").value = document.getElementById("js-makedQty").value.replace(/,/g,"");

        document.getElementById("js-badQty").value = document.getElementById("js-badQty").value.replace(/,/g,"");

        $("#js-useTime")[0].value = document.getElementById("js-useTime").value.replace(/,/g,"");


    }

</script>
</body>
</html>

