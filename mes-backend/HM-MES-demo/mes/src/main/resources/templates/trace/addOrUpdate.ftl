<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>添加/修改 溯源</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <#include "${request.contextPath}/common/common.ftl">
    <link href="${request.contextPath}/css/effect.css" rel="stylesheet" type="text/css"/>

    <style>

    </style>

</head>
<body>
<div class="splayui-container">
    <div class="splayui-main">
        <form class="layui-form splayui-form" lay-filter="formTest">
            <div class="layui-row">
                <div class="layui-col-xs6 layui-col-sm6 layui-col-md6">


                    <div class="layui-form-item">
                        <label for="js-productId" class="layui-form-label ">产品编码
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-productId" name="productId"
                                   autocomplete="off" class="layui-input" value="${result.productId}">
                        </div>
                    </div>


                    <div class="layui-form-item">
                        <label for="js-productSerial" class="layui-form-label sp-required">产品序列码
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-productSerial" name="productSerial" lay-verify="required" autocomplete="off"
                                   class="layui-input" value="${result.productSerial}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-material" class="layui-form-label ">物料编码
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-material" name="material"
                                   autocomplete="off" class="layui-input" value="${result.material}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-materialBatchNo" class="layui-form-label ">物料批次
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-materialBatchNo" name="materialBatchNo"
                                   autocomplete="off" class="layui-input" value="${result.materialBatchNo}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-bomCode" class="layui-form-label ">BOM编码
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-bomCode" name="bomCode"
                                   autocomplete="off" class="layui-input" value="${result.bomCode}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-type" class="layui-form-label ">产品规格
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-type" name="type"
                                   autocomplete="off" class="layui-input" value="${result.type}">
                        </div>
                    </div>



                </div>

                <div class="layui-form-item layui-hide">
                    <div class="layui-input-block">
                        <input id="js-id" name="id" value="${result.id}"/>
                        <button id="js-submit" class="layui-btn" lay-submit lay-filter="js-submit-filter">确定
                        </button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['form', 'util'], function () {
        var form = layui.form,
            util = layui.util;

        //监听提交
        form.on('submit(js-submit-filter)', function (data) {
            //alert( JSON.stringify(data.field));

            spUtil.submitForm({
                //contentType: 'application/json;charset=UTF-8',  //@RequestBody  要对应起来，contentType默认按 x-from 处理得
                url: "${request.contextPath}/trace/add-or-update",
                data: data.field
            });
            return false;
        });

    });
</script>
</body>
</html>

