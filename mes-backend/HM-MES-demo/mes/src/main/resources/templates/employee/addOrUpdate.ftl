<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>添加/修改group</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <#include "${request.contextPath}/common/common.ftl">
    <link href="${request.contextPath}/css/effect.css" rel="stylesheet" type="text/css"/>

    <style>

    </style>

</head>
<body>
<div class="splayui-container">
    <div class="splayui-main">
        <form class="layui-form splayui-form" lay-filter="formTest">
            <div class="layui-row">
                <div class="layui-col-xs6 layui-col-sm6 layui-col-md6">

                    <div class="layui-form-item">
                        <label for="js-employeeName" class="layui-form-label sp-required">姓名
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-employeeName" name="employeeName" lay-verify="required" autocomplete="off"
                                   class="layui-input" value="${result.employeeName}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-groupName" class="layui-form-label ">班组
                        </label>
                        <div class="layui-input-inline">
                            <select id="js-groupName" name="groupName" lay-filter="groupName-filter">
                            </select>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-im" class="layui-form-label ">im
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-im" name="im"
                                   autocomplete="off" class="layui-input" value="${result.im}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-contact" class="layui-form-label ">联系方式
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-contact" name="contact"
                                   autocomplete="off" class="layui-input" value="${result.contact}">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label for="js-gender" class="layui-form-label ">性别
                        </label>
                        <div class="layui-input-inline">
                            <input type="text" id="js-gender" name="gender"
                                   autocomplete="off" class="layui-input" value="${result.gender}">
                        </div>
                    </div>



                </div>

                <div class="layui-form-item layui-hide">
                    <div class="layui-input-block">
                        <input id="js-id" name="id" value="${result.id}"/>
                        <button id="js-submit" class="layui-btn" lay-submit lay-filter="js-submit-filter">确定
                        </button>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['form', 'util'], function () {
        var form = layui.form,
            util = layui.util;

        geGroupData();

        //监听提交
        form.on('submit(js-submit-filter)', function (data) {
            //alert( JSON.stringify(data.field));

            spUtil.submitForm({
                //contentType: 'application/json;charset=UTF-8',  //@RequestBody  要对应起来，contentType默认按 x-from 处理得
                url: "${request.contextPath}/employee/add-or-update",
                data: data.field
            });
            return false;
        });


        //给表单赋值
        form.val("formTest", { //formTest 即 class="layui-form" 所在元素属性 lay-filter="" 对应的值

            "groupName": "${result.groupName}"

        });


    });


    function geGroupData() {
        spUtil.ajax({
            url: '${request.contextPath}/group/list',
            async: false,
            type: 'GET',
            // 是否显示 loading
            showLoading: true,
            // 是否序列化参数
            serializable: false,
            // 参数
            data: {},
            success: function (data) {
                typeRows = data.data;

                $.each(typeRows, function (index, item) {
                    $('#js-groupName').append(new Option(item.name, item.name));
                });

            }
        });

    }





</script>
</body>
</html>

