package com.lgl.mes.common.util.org.easy.excel.aop.cell.inner;

import org.apache.commons.lang3.StringUtils;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellInterceptorAdapter;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellJoinPoint;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelException;

/**
 * 处理isNull标签
 * @author lisuo
 *
 */
public class IsNullCellInterceptor extends CellInterceptorAdapter {

	@Override
	public boolean supports(CellJoinPoint joinPoint) {
		return !joinPoint.getFieldValue().isNull();
	}

	@Override
	public Object executeImport(CellJoinPoint joinPoint) throws ExcelException {
		Object value = joinPoint.getValue();
		if (value == null || (value instanceof String && StringUtils.isBlank(value.toString()))) {
			throw newExcelDataException("不能为空", joinPoint);
		}
		return super.executeImport(joinPoint);
	}

}
