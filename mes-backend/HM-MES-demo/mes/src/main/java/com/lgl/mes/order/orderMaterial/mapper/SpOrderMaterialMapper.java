package com.lgl.mes.order.orderMaterial.mapper;

import com.lgl.mes.order.orderMaterial.entity.SpOrderMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 线体表 Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-10-01
 */
public interface SpOrderMaterialMapper extends BaseMapper<SpOrderMaterial> {

}
