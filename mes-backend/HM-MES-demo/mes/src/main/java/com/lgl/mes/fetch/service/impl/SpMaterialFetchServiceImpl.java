package com.lgl.mes.fetch.service.impl;

import com.lgl.mes.fetch.entity.SpMaterialFetch;
import com.lgl.mes.fetch.mapper.SpMaterialFetchMapper;
import com.lgl.mes.fetch.service.ISpMaterialFetchService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 基础物料表 服务实现类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-12-10
 */
@Service
public class SpMaterialFetchServiceImpl extends ServiceImpl<SpMaterialFetchMapper, SpMaterialFetch> implements ISpMaterialFetchService {

}
