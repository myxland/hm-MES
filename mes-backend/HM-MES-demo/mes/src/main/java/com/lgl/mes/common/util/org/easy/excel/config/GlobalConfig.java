package com.lgl.mes.common.util.org.easy.excel.config;

import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellInterceptor;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.inner.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Excel 全局配置
 * @author lisuo
 *
 */
public class GlobalConfig {
	
	private List<CellInterceptor> cellInterceptors = new ArrayList<>();
	
	private static GlobalConfig instance = new GlobalConfig();
	
	private GlobalConfig() {
		cellInterceptors.add(new DefaultValueCellInterceptor());
		cellInterceptors.add(new TrimStringCellInterceptor());
		cellInterceptors.add(new IsNullCellInterceptor());
		cellInterceptors.add(new RegexCellInterceptor());
		cellInterceptors.add(new PatternCellInterceptor());
		cellInterceptors.add(new FormatCellInterceptor());
		cellInterceptors.add(new DecimalFormatCellInterceptor());
		cellInterceptors.add(new AfterCellInterceptor());
		cellInterceptors.add(new CustomCellInterceptor());
	}
	
	public static GlobalConfig getInstance() {
		return instance;
	}
	
	public List<CellInterceptor> getCellInterceptors() {
		return cellInterceptors;
	}
	
}
