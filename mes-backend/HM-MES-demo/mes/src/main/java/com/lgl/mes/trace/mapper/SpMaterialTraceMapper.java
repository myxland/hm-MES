package com.lgl.mes.trace.mapper;

import com.lgl.mes.trace.entity.SpMaterialTrace;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-27
 */
public interface SpMaterialTraceMapper extends BaseMapper<SpMaterialTrace> {

}
