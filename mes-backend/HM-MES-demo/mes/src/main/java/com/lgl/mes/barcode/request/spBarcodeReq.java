package com.lgl.mes.barcode.request;

import com.lgl.mes.common.BasePageReq;

/**
 * 分页对象
 * @author lgl  75039960@qq.com
 * @since 2022/08/01
 */
public class spBarcodeReq extends BasePageReq {

    /**
     *模糊查询device
     */
    private String codeLike;

    public String getCodeLike() {
        return this.codeLike;
    }

    public void setCodeLike(String codeLike)  { this.codeLike= codeLike;}

}
