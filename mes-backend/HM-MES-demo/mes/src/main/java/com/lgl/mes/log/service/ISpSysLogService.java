package com.lgl.mes.log.service;

import com.lgl.mes.log.entity.SpSysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 日志  服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-09
 */
public interface ISpSysLogService extends IService<SpSysLog> {

}
