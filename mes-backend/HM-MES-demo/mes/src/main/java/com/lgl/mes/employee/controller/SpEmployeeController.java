package com.lgl.mes.employee.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lgl.mes.common.Result;
import com.lgl.mes.common.util.log.SysLog;
import com.lgl.mes.config.entity.SpConfig;
import com.lgl.mes.device.entity.SpDevice;
import com.lgl.mes.employee.entity.SpEmployee;
import com.lgl.mes.employee.request.SpEmployeeReq;
import com.lgl.mes.employee.service.ISpEmployeeService;
import com.lgl.mes.group.entity.SpGroup;
import com.lgl.mes.group.request.SpGroupReq;
import com.lgl.mes.group.service.ISpGroupService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import com.lgl.mes.common.BaseController;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 工序表 前端控制器
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-28
 */
@Controller
@RequestMapping("/employee")
public class SpEmployeeController extends BaseController {


    @Autowired
    public ISpEmployeeService iSpEmployeeService;

    /**
     * 设备管理界面
     *
     * @param model 模型
     * @return 设备管理界面
     */
    @ApiOperation("UI")
    @ApiImplicitParams({@ApiImplicitParam(name = "model", value = "模型", defaultValue = "模型")})
    @GetMapping("/list-ui")
    public String listUI(Model model) {
        return "employee/list";
    }


    /**
     *config编辑界面
     *
     * @param model  模型
     * @param record 平台表对象
     * @return 更改界面
     */
    @ApiOperation("管理编辑界面")
    @GetMapping("/add-or-update-ui")
    public String addOrUpdateUI(Model model, SpEmployee record) throws Exception {
        if (StringUtils.isNotEmpty(record.getId())) {
            SpEmployee spEmployee = iSpEmployeeService.getById(record.getId());
            model.addAttribute("result", spEmployee);
        }
        return "employee/addOrUpdate";
    }


    /**
     * 分页查询
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("信息分页查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "请求参数", defaultValue = "请求参数")})
    @PostMapping("/page")
    @ResponseBody
    public Result page(SpEmployeeReq req) {
        QueryWrapper<SpEmployee> queryWrapper =new QueryWrapper();

        if (StringUtils.isNotEmpty(req.getEmployeeNameLike()))
        {
            queryWrapper.like("name",req.getEmployeeNameLike());
        }

        IPage result = iSpEmployeeService.page(req,queryWrapper);
        return Result.success(result);
    }


    /**
     *
     *
     * @param spEmployee 流程与工序DTO
     * @return 执行结果
     */
    @SysLog("修改员工记录")
    @ApiOperation("新增+修改")
    @PostMapping("/add-or-update")
    @ResponseBody
    public Result addOrUpdate(/*@RequestBody*/ SpEmployee spEmployee) throws Exception {
        iSpEmployeeService.saveOrUpdate(spEmployee);

        return Result.success();
    }

    /**
     * 删除流程
     *
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @SysLog("delete 雇员记录")
    @ApiOperation("delete记录")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "实体", defaultValue = "实体")})
    @PostMapping("/delete")
    @ResponseBody
    public Result deleteByTableNameId(SpEmployee req) throws Exception {

        iSpEmployeeService.removeById(req.getId());

        return Result.success();
    }




}
