package com.lgl.mes.barcode.request;

import com.lgl.mes.common.BasePageReq;

/**
 * 分页对象
 * @author lgl  75039960@qq.com
 * @since 2022/08/01
 */
public class spBarcodeBatchAddReq extends BasePageReq {

    /**
     *模糊查询device
     */
    private String baseUrl;   //用于构建二维码 ：  http://www.dreammmm.net?code=your code&p=0
    private Integer count;   //生成记录数量
    private String param;   //参数

    private Integer width;   // image
    private Integer height;   // image  height

    private String directUrl;   //跳转目的URL，这里我们认为是官网, 用于设置前端跳转链接

    public String getBaseUrl() {
        return this.baseUrl;
    }
    public void setBaseUrl(String baseUrl)  { this.baseUrl= baseUrl;}


    public String getDirectUrl() {
        return this.directUrl;
    }
    public void setDirectUrl(String directUrl)  { this.directUrl= directUrl;}

    public Integer getCount() {
        return this.count;
    }
    public void setCount(Integer count)  { this.count= count;}

    public String getParam() {
        return this.param;
    }
    public void setParam(String param)  { this.param= param;}


    public Integer getWidth() {
        return this.width;
    }
    public void setWidth(Integer width)  { this.width= width;}
    public Integer getHeight() {
        return this.height;
    }
    public void setHeight(Integer height)  { this.height= height;}

}
