package com.lgl.mes.order.orderMaterial.request;

import com.lgl.mes.common.BasePageReq;

/**
 * 分页对象
 * @author lgl  75039960@qq.com
 * @since 2022/04/01
 */
public class spOrderMaterialReq extends BasePageReq {

    /**
     *模糊查询product
     */
    private String lineLike;

    private String orderCodeLike;
    private String bomCodeLike;

    private String materialCodeLike;

    private String materialBatchNoLike;


    public String getMaterialBatchNoLike() {
        return this.materialBatchNoLike;
    }

    public String getLineLike() {
        return this.lineLike;
    }

    public String getMaterialCodeLike() {
        return this.materialCodeLike;
    }

    public String getOrderCodeLike() {
        return this.orderCodeLike;
    }

    public String getBomCodeLike() {
        return this.bomCodeLike;
    }


    public void setMaterialCodeLike(String materialCodeLike) {
        this.materialCodeLike = materialCodeLike;
    }


    public void setLineLike(String lineLike)  { this.lineLike= lineLike;}

    public void setOrderCodeLike(String orderCodeLike)  { this.orderCodeLike= orderCodeLike;}

    public void setBomCodeLike(String bomCodeLike) {
        this.bomCodeLike = bomCodeLike;
    }

    public void setMaterialBatchNoLike(String materialBatchNoLike) {
        this.materialBatchNoLike = materialBatchNoLike;
    }

}
