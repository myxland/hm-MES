package com.lgl.mes.config.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lgl.mes.common.BaseController;
import com.lgl.mes.common.Result;
import com.lgl.mes.common.util.log.SysLog;
import com.lgl.mes.config.entity.SpConfig;
import com.lgl.mes.config.request.spConfigReq;
import com.lgl.mes.config.service.ISpConfigService;
import com.lgl.mes.device.entity.SpDevice;
import com.lgl.mes.device.request.spDeviceReq;
import com.lgl.mes.line.entity.SpLine;
import com.lgl.mes.protocol.entity.SpComProtocol;
import com.lgl.mes.protocol.service.ISpComProtocolService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-23
 */
@Controller
@RequestMapping("/config")
public class SpConfigController extends BaseController {

    @Autowired
    public ISpConfigService iSpConfigService;

    @Autowired
    public ISpComProtocolService iSpComProtocolService;




    /**
     * 设备管理界面
     *
     * @param model 模型
     * @return 设备管理界面
     */
    @ApiOperation("设备UI")
    @ApiImplicitParams({@ApiImplicitParam(name = "model", value = "模型", defaultValue = "模型")})
    @GetMapping("/list-ui")
    public String listUI(Model model) {
        return "config/list";
    }


    /**
     *config编辑界面
     *
     * @param model  模型
     * @param record 平台表对象
     * @return 更改界面
     */
    @ApiOperation("config管理编辑界面")
    @GetMapping("/add-or-update-ui")
    public String addOrUpdateUI(Model model, SpDevice record) throws Exception {
        if (StringUtils.isNotEmpty(record.getId())) {
            SpConfig spConfig = iSpConfigService.getById(record.getId());
            model.addAttribute("result", spConfig);
        }
        return "config/addOrUpdate";
    }


    /**
     * 分页查询
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("config信息分页查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "请求参数", defaultValue = "请求参数")})
    @PostMapping("/page")
    @ResponseBody
    public Result page(spConfigReq req) {
        QueryWrapper<SpConfig> queryWrapper =new QueryWrapper();

        if (StringUtils.isNotEmpty(req.getLineIdLike()))
        {
            queryWrapper.like("line_id",req.getLineIdLike());
        }

        IPage result = iSpConfigService.page(req,queryWrapper);
        return Result.success(result);
    }


    /**
     *
     *
     * @param spConfig 流程与工序DTO
     * @return 执行结果
     */
    @SysLog("新增+修改config记录")
    @ApiOperation("新增+修改")
    @PostMapping("/add-or-update")
    @ResponseBody
    public Result addOrUpdate(/*@RequestBody*/ SpConfig spConfig) throws Exception {
        iSpConfigService.saveOrUpdate(spConfig);

        return Result.success();
    }

    /**
     * 删除流程
     *
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @SysLog("delete配置记录")
    @ApiOperation("delete配置记录")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "配置实体", defaultValue = "配置实体")})
    @PostMapping("/delete")
    @ResponseBody
    public Result deleteByTableNameId(SpConfig req) throws Exception {

        iSpConfigService.removeById(req.getId());

        return Result.success();
    }



    @ResponseBody
    @RequestMapping(value = "/type/list", method = RequestMethod.GET, produces = "application/json")
    public Result getTypes() throws Exception {


        List<SpComProtocol> list = iSpComProtocolService.list();

        return Result.success(list);

    }



}
