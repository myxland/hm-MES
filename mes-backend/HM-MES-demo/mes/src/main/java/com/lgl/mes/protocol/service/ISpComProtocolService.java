package com.lgl.mes.protocol.service;

import com.lgl.mes.protocol.entity.SpComProtocol;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 线体表 服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-08-30
 */
public interface ISpComProtocolService extends IService<SpComProtocol> {

}
