package com.lgl.mes.employee.request;

import com.lgl.mes.common.BasePageReq;

/**
 * 分页对象
 * @author lgl  75039960@qq.com
 * @since 2022/08/01
 */
public class SpEmployeeReq extends BasePageReq {

    /**
     *模糊查询
     */
    private String employeeNameLike;

    public String getEmployeeNameLike() {
        return this.employeeNameLike;
    }
    public void setEmployeeNameLike(String employeeNameLike)  { this.employeeNameLike= employeeNameLike;}


}
