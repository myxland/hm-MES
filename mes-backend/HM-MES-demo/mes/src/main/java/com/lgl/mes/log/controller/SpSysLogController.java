package com.lgl.mes.log.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lgl.mes.common.Result;
import com.lgl.mes.common.util.log.SysLog;
import com.lgl.mes.config.entity.SpConfig;
import com.lgl.mes.config.request.spConfigReq;
import com.lgl.mes.config.service.ISpConfigService;
import com.lgl.mes.device.entity.SpDevice;
import com.lgl.mes.log.entity.SpSysLog;
import com.lgl.mes.log.request.spLogReq;
import com.lgl.mes.log.service.ISpSysLogService;
import com.lgl.mes.protocol.entity.SpComProtocol;
import com.lgl.mes.protocol.service.ISpComProtocolService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import com.lgl.mes.common.BaseController;

import java.util.List;

/**
 * <p>
 * 日志  前端控制器
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-09
 */
@Controller
@RequestMapping("/log")
public class SpSysLogController extends BaseController {

    @Autowired
    public ISpSysLogService iSpSysLogService;


    /**
     * 设备管理界面
     *
     * @param model 模型
     * @return 设备管理界面
     */
    @ApiOperation("log UI")
    @ApiImplicitParams({@ApiImplicitParam(name = "model", value = "模型", defaultValue = "模型")})
    @GetMapping("/list-ui")
    public String listUI(Model model) {
        return "log/list";
    }


    /**
     *config编辑界面
     *
     * @param model  模型
     * @param record 平台表对象
     * @return 更改界面
     */
    @ApiOperation("管理编辑界面")
    @GetMapping("/add-or-update-ui")
    public String addOrUpdateUI(Model model, SpSysLog record) throws Exception {
        if (StringUtils.isNotEmpty(record.getId())) {
            SpSysLog spSysLog = iSpSysLogService.getById(record.getId());
            model.addAttribute("result", spSysLog);
        }
        return "log/addOrUpdate";
    }


    /**
     * 分页查询
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("信息分页查询")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "请求参数", defaultValue = "请求参数")})
    @PostMapping("/page")
    @ResponseBody
    public Result page(spLogReq req) {
        QueryWrapper<SpSysLog> queryWrapper =new QueryWrapper();

        if (StringUtils.isNotEmpty(req.getUsernameLike()))
        {
            queryWrapper.like("user_name",req.getUsernameLike());
        }

        if (StringUtils.isNotEmpty(req.getMethodLike()))
        {
            queryWrapper.like("method",req.getMethodLike());
        }
        if (StringUtils.isNotEmpty(req.getOperationLike()))
        {
            queryWrapper.like("operation",req.getOperationLike());
        }


        IPage result = iSpSysLogService.page(req,queryWrapper);
        return Result.success(result);
    }


    /**
     *
     *
     * @param spSysLog 流程与工序DTO
     * @return 执行结果
     */
    @ApiOperation("新增+修改")
    @PostMapping("/add-or-update")
    @ResponseBody
    public Result addOrUpdate(/*@RequestBody*/ SpSysLog spSysLog) throws Exception {
        iSpSysLogService.saveOrUpdate(spSysLog);

        return Result.success();
    }

    /**
     * 删除流程
     *
     *
     * @param req 请求参数
     * @return Result 执行结果
     */
    @ApiOperation("delete配置记录")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "配置实体", defaultValue = "配置实体")})
    @PostMapping("/delete")
    @ResponseBody
    public Result deleteByTableNameId(SpSysLog req) throws Exception {

        iSpSysLogService.removeById(req.getId());

        return Result.success();
    }


    @ApiOperation("批量删除计划信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "ids", value = "play ids", defaultValue = "plan ids")})
    @PostMapping("/batchDelete")
    @ResponseBody
    public Result batchDelete( String  ids) throws Exception {

        String[] tempList = ids.split(",");
        for (String id  :  tempList)
        {
            iSpSysLogService.removeById(id);
        }


        return Result.success();
    }


    @SysLog("删除所有日志")
    @ApiOperation("delete  all log记录")
    @ApiImplicitParams({@ApiImplicitParam(name = "req", value = "配置实体", defaultValue = "配置实体")})
    @PostMapping("/allDelete")
    @ResponseBody
    public Result deleteALL(SpSysLog req) throws Exception {
        QueryWrapper<SpSysLog> queryWrapper =new QueryWrapper();

        iSpSysLogService.remove(queryWrapper);

        return Result.success();
    }


}
