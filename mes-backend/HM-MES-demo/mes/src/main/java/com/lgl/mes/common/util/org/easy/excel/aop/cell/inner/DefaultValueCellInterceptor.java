package com.lgl.mes.common.util.org.easy.excel.aop.cell.inner;

import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellInterceptorAdapter;
import com.lgl.mes.common.util.org.easy.excel.aop.cell.CellJoinPoint;
import com.lgl.mes.common.util.org.easy.excel.exception.ExcelException;

/**
 * 处理defaultValue标签
 * @author lisuo
 *
 */
public class DefaultValueCellInterceptor extends CellInterceptorAdapter {

	@Override
	public boolean supports(CellJoinPoint joinPoint) {
		return joinPoint.getFieldValue().getDefaultValue() != null;
	}

	@Override
	public Object executeImport(CellJoinPoint joinPoint) throws ExcelException {
		if (joinPoint.getValue() == null) {
			return joinPoint.getFieldValue().getDefaultValue();
		}
		return super.executeImport(joinPoint);
	}

	@Override
	public Object executeExport(CellJoinPoint joinPoint) throws ExcelException {
		if (joinPoint.getValue() == null) {
			return joinPoint.getFieldValue().getDefaultValue();
		}
		return super.executeExport(joinPoint);
	}
}
