package com.lgl.mes.common.util;

import com.wuwenze.poi.convert.ReadConverter;
import com.wuwenze.poi.exception.ExcelKitWriteConverterException;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;

/**
 * Execl导出时间类型字段格式化
 *
 * @author tanghaiyang
 */
@Slf4j
public class ReadsConverter implements ReadConverter {

    @Override
    public Object convert(Object value){
        if (value == null)
            return "";
        else {
            try {
                return DateUtil.StrToDate(value.toString());
            } catch (ParseException e) {
                String message = "时间转换异常";
                log.error(message, e);
                throw new ExcelKitWriteConverterException(message);
            }
        }
    }
}
