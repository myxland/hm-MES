package com.lgl.mes.common.util.org.easy.excel.aop.cell;

import com.lgl.mes.common.util.org.easy.excel.config.ExcelDefinition;
import com.lgl.mes.common.util.org.easy.excel.config.FieldValue;
import org.springframework.beans.AbstractPropertyAccessor;
import org.springframework.context.ApplicationContext;

/**
 * Cell 列拦截器切入点
 * @author lisuo
 *
 */
public class CellJoinPoint {
	
	/** Excel当前节点配置信息 */
	private ExcelDefinition excelDefinition;
	 
	/** Excel当前字段配置信息 */
	private FieldValue fieldValue;
	
	/** pojo属性访问器 */
	private AbstractPropertyAccessor accessor;
	 
	/** 当前实例pojo */
	private Object refObject;
	 
	/** 当前正在处理的行号 */
	private int rowNum;
	 
	/** 当前正在处理的value */
	private Object value;
	 
	/** 当前正在处理的原生value(从poi解析的原生value,没有经过转换器的) */
	private Object originalValue;
	
	/** spring 环境上下文,不是spring环境为空 */
	private ApplicationContext ctx;
	

	public CellJoinPoint(ExcelDefinition excelDefinition, FieldValue fieldValue, AbstractPropertyAccessor accessor,
			Object refObject, int rowNum, Object value, Object originalValue, ApplicationContext ctx) {
		super();
		this.excelDefinition = excelDefinition;
		this.fieldValue = fieldValue;
		this.accessor = accessor;
		this.refObject = refObject;
		this.rowNum = rowNum;
		this.value = value;
		this.originalValue = originalValue;
		this.ctx = ctx;
	}

	public ExcelDefinition getExcelDefinition() {
		return excelDefinition;
	}

	public void setExcelDefinition(ExcelDefinition excelDefinition) {
		this.excelDefinition = excelDefinition;
	}

	public FieldValue getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(FieldValue fieldValue) {
		this.fieldValue = fieldValue;
	}

	public AbstractPropertyAccessor getAccessor() {
		return accessor;
	}

	public void setAccessor(AbstractPropertyAccessor accessor) {
		this.accessor = accessor;
	}

	public Object getRefObject() {
		return refObject;
	}

	public void setRefObject(Object refObject) {
		this.refObject = refObject;
	}

	public int getRowNum() {
		return rowNum;
	}

	public void setRowNum(int rowNum) {
		this.rowNum = rowNum;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Object getOriginalValue() {
		return originalValue;
	}

	public void setOriginalValue(Object originalValue) {
		this.originalValue = originalValue;
	}

	public ApplicationContext getCtx() {
		return ctx;
	}

	public void setCtx(ApplicationContext ctx) {
		this.ctx = ctx;
	}
	
	
}
