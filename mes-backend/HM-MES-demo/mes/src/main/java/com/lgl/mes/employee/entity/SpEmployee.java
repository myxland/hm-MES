package com.lgl.mes.employee.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lgl.mes.common.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 工序表
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-28
 */
@ApiModel(value="SpEmployee对象", description="工序表")
public class SpEmployee extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "姓名")
    private String employeeName;

    @ApiModelProperty(value = "所属班组")
    private String groupName;

    @ApiModelProperty(value = "im 通讯id，可用于下达工单发送消息")
    private String im;

    @ApiModelProperty(value = "联系方式")
    private String contact;

    @ApiModelProperty(value = "图像url")
    private String image;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "所属部门")
    private String department;

    @ApiModelProperty(value = "逻辑删除：1 表示删除，0 表示未删除，2 表示禁用")
    private String isDeleted;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }
    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    //@Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "SpEmployee{" +
                "employeeName=" + employeeName +
                ", groupName=" + groupName +
                ", im=" + im +
                ", contact=" + contact +
                ", image=" + image +
                ", gender=" + gender +
                ", age=" + age +
                ", department=" + department +
                ", isDeleted=" + isDeleted +
                "}";
    }
}
