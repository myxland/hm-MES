package com.lgl.mes.common.exception;

import lombok.Data;

/**
 * 自定义异常类
 */
@Data
public class MyException extends RuntimeException {

    private String msg;

    public MyException(String msg){
        super(msg);
        this.msg=msg;
    }
}
