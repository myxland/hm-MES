package com.lgl.mes.order.entity;


import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Excel("物料表")
public class OrderMaterialExcel {

   /*
   这里的定义顺序决定了，excel表字段的解释顺序
    */

    @ExcelField(value = "生产线")
    private String line;

    /**
     * 工单编码
     */
    @ExcelField(value = "工单编码")
    private String orderCode;


    /**
     * BOM编码
     *
     */
    @ExcelField(value = "BOM编码")
    private String bomCode;


    /**
     * 物料类型
     */
    @ExcelField(value = "物料编码")
    private String materialCode;

    /**
     * 批次
     */
    @ExcelField(value = "物料批次")
    private String batchNo;


    @ExcelField(value = "数量")
    private Integer amount;


}
