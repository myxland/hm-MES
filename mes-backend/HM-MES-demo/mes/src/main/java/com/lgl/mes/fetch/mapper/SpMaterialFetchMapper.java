package com.lgl.mes.fetch.mapper;

import com.lgl.mes.fetch.entity.SpMaterialFetch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 基础物料表 Mapper 接口
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-12-10
 */
public interface SpMaterialFetchMapper extends BaseMapper<SpMaterialFetch> {

}
