package com.lgl.mes.basedata.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lgl.mes.basedata.entity.MaterialExcel;
import com.lgl.mes.basedata.entity.SpMaterile;
import com.lgl.mes.basedata.mapper.SpMaterileMapper;
import com.lgl.mes.basedata.service.ISpMaterileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lgl.mes.common.util.org.easy.excel.ExcelContext;
import com.lgl.mes.common.util.org.easy.excel.result.ExcelImportResult;
import com.lgl.mes.trace.entity.SpMaterialTrace;
import com.wuwenze.poi.ExcelKit;
import com.wuwenze.poi.handler.ExcelReadHandler;
import com.wuwenze.poi.pojo.ExcelErrorField;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author  7503o9960@qq.com
 * @since 2022-08-19
 */
@Service
public class SpMaterileServiceImpl extends ServiceImpl<SpMaterileMapper, SpMaterile> implements ISpMaterileService {

    //@Override
    public String LocalImport1(MultipartFile file) {
        String message = "";
        List<MaterialExcel> successList = new ArrayList();
        List<Map<String, Object>> errorList = new ArrayList();
        if (file.isEmpty()) {
            throw new FileNotEmptyException("上传文件不能为空");
        }
        try {
            ExcelKit.$Import(MaterialExcel.class)
                    .readXlsx(file.getInputStream(), new ExcelReadHandler<MaterialExcel>() {
                        @Override
                        public void onSuccess(int i, int i1, MaterialExcel materialExcel) {
                            successList.add(materialExcel); // 单行读取成功，加入入库队列。
                        }
                        @Override
                        public void onError(int sheetIndex, int rowIndex, List<ExcelErrorField> list) {
                            // 读取数据失败，记录了当前行所有失败的数据
                            Map<String, Object> map = new HashMap<>();
                            map.put("sheetIndex", sheetIndex);
                            map.put("rowIndex", rowIndex);
                            map.put("errorFields", list);
                            errorList.add(map);
                        }
                    });


            successList.stream().forEach(item ->{
                //ese.setCreateDate(new Date());
                //.setCreateBy(UserUtil.getUserId());
                JSONObject jsonObject = (JSONObject) JSONObject.toJSON(item);
                String materiel  = jsonObject.getString("materiel");
                QueryWrapper<SpMaterile> wrapper=new QueryWrapper<>();
                wrapper.eq("materiel", materiel);
                if (this.list(wrapper).size()==0) {   //去重
                    SpMaterile spMaterile = new SpMaterile(jsonObject);
                    this.baseMapper.insert(spMaterile);
                }
            });
        } catch (Exception e) {
            message = e.getMessage();
            e.printStackTrace();
        }
        return message;
    }


    @Override
    public String LocalImport(MultipartFile file) {

        String message = "上传完毕";
        if (file.isEmpty()) {
        throw new FileNotEmptyException("上传文件不能为空");
        }

        try {
            String config = "excel/mes-excel-config.xml";
            ExcelContext context = new ExcelContext(config);
            // Excel配置文件中配置的id
            String excelId = "material";

            ExcelImportResult result = context.readExcel(excelId, 0, file.getInputStream());
            List<SpMaterile>  items = result.getListBean();
            for(SpMaterile item:items){
                QueryWrapper<SpMaterile> wrapper=new QueryWrapper<>();
                wrapper.eq("materiel", item.getMateriel());
                if (this.list(wrapper).size()==0) {
                    this.baseMapper.insert(item);
                }
            }

        } catch (Exception e) {
            message = e.getMessage();
            e.printStackTrace();
        }

        return message;
}



}
