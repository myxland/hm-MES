package com.lgl.mes.trace.service;

import com.lgl.mes.trace.entity.SpMaterialTrace;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 75039960@qq.com
 * @since 2022-09-27
 */
public interface ISpMaterialTraceService extends IService<SpMaterialTrace> {
    public String LocalImport(MultipartFile file);

}
