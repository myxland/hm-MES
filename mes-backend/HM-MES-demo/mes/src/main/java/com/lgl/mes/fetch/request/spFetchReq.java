package com.lgl.mes.fetch.request;

import com.lgl.mes.common.BasePageReq;

/**
 * 分页对象
 * @author lgl  75039960@qq.com
 * @since 2022/12/10
 */
public class spFetchReq extends BasePageReq {

    /**
     *模糊查询  mmaterial
     */
    private String materialLike;

    public String getMaterialLike() {
        return this.materialLike;
    }

    public void setMaterialLike(String materialLike)  { this.materialLike= materialLike;}

}
